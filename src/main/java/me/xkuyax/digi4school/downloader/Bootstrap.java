package me.xkuyax.digi4school.downloader;

import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.cookie.Cookie;
import org.apache.http.cookie.CookieOrigin;
import org.apache.http.cookie.CookieSpecProvider;
import org.apache.http.cookie.MalformedCookieException;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.cookie.DefaultCookieSpec;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

public class Bootstrap {
    
    private static Path OUTPUT_PATH = Paths.get("data");
    private static CloseableHttpClient httpClient;
    private static int pages;
    private static String bookId;
    
    public static void main(String[] args) throws Exception {
        if (args.length < 4) {
            System.out.println("Usage: java -jar digi4schooldownloader.jar user password bookId pages");
            return;
        }
        String user = args[0];
        String password = args[1];
        bookId = args[2];
        pages = Integer.parseInt(args[3]);
        createHttpClient();
        Files.walkFileTree(Paths.get("data"), new SimpleFileVisitor<Path>() {
        
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return super.visitFile(file, attrs);
            }
        
            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                Files.delete(dir);
                return super.postVisitDirectory(dir, exc);
            }
        });
        Files.createDirectory(Paths.get("data"));
        HttpPost httpPost = new HttpPost("https://digi4school.at/br/xhr/login");
        List<NameValuePair> nvps = new ArrayList<>();
        nvps.add(new BasicNameValuePair("email", user));
        nvps.add(new BasicNameValuePair("password", password));
        httpPost.setEntity(new UrlEncodedFormEntity(nvps));
        try (CloseableHttpResponse httpPostResponse = httpClient.execute(httpPost)) {
            EntityUtils.consume(httpPostResponse.getEntity());
            try (CloseableHttpResponse httpResponse = httpClient.execute(new HttpGet("https://digi4school.at/ebooks"))) {
                String response = EntityUtils.toString(httpResponse.getEntity());
                Document document = Jsoup.parse(response);
                Element element = document.getElementById("shelf").select("a").last();
                String href = element.attr("href");
                try (CloseableHttpResponse closeableHttpResponse = httpClient.execute(new HttpGet("https://digi4school.at/" + href))) {
                    String a = EntityUtils.toString(closeableHttpResponse.getEntity());
                    fakeClick(Jsoup.parse(a));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        StringBuilder stringBuilder = new StringBuilder("rsvg-convert -f pdf -o out.pdf ");
        for (int i = 1; i <= pages; i++) {
            stringBuilder.append(i);
            stringBuilder.append(".svg ");
        }
        Files.write(Paths.get("data/convert.sh"), stringBuilder.toString().replaceAll("\r\n", "\n").getBytes());
    }
    
    public static void createHttpClient() {
        Registry<CookieSpecProvider> r = RegistryBuilder.<CookieSpecProvider>create().register("easy", (context) -> new EasyCookieSpec()).build();
        BasicCookieStore cookieStore = new BasicCookieStore();
        RequestConfig requestConfig = RequestConfig.custom().setCookieSpec("easy").build();
        httpClient = HttpClients.custom().setDefaultCookieStore(cookieStore).setDefaultCookieSpecRegistry(r).setDefaultRequestConfig(requestConfig).build();
    }
    
    public static void fakeClick(Document document) throws IOException {
        try (CloseableHttpResponse httpResponse = httpClient.execute(httpPostFromElement(document.getElementById("lti")))) {
            String response = EntityUtils.toString(httpResponse.getEntity());
            fakeClick2(Jsoup.parse(response));
        }
    }
    
    public static void fakeClick2(Document document) throws IOException {
        try (CloseableHttpResponse httpResponse = httpClient.execute(httpPostFromElement(document.getElementById("lti")))) {
            String response = EntityUtils.toString(httpResponse.getEntity());
            fakeClick3(Jsoup.parse(response).select("A").first().attr("href"));
        }
    }
    
    public static HttpPost httpPostFromElement(Element element) throws UnsupportedEncodingException {
        String url = element.attr("action");
        HttpPost httpPost = new HttpPost(url);
        List<NameValuePair> nameValuePairs = new ArrayList<>();
        element.children().forEach(element1 -> {
            nameValuePairs.add(new BasicNameValuePair(element1.attr("name"), element1.attr("value")));
        });
        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        return httpPost;
    }
    
    public static void fakeClick3(String url) {
        HttpGet httpGet = new HttpGet(url);
        try (CloseableHttpResponse httpResponse = httpClient.execute(httpGet)) {
            EntityUtils.consume(httpResponse.getEntity());
            try (CloseableHttpResponse codeHttpResponse = httpClient.execute(new HttpGet(url + "code_request"))) {
                String code = EntityUtils.toString(codeHttpResponse.getEntity());
                try (CloseableHttpResponse codeParaHttpResponse = httpClient.execute(new HttpGet("https://para.digi4school.at/para/" + code))) {
                    EntityUtils.consume(codeParaHttpResponse.getEntity());
                    try (CloseableHttpResponse firstPageHttpResponse = httpClient.execute(new HttpGet(url + "1.html"))) {
                        EntityUtils.consume(firstPageHttpResponse.getEntity());
                        for (int i = 1; i <= pages; i++) {
                            String svgUrl = "http://a.digi4school.at/ebook/" + bookId + "/" + i + "/";
                            downloadAllShit(i + "", svgUrl);
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static void downloadAllShit(String id, String svg) {
        String svgFile = svg + id + ".svg";
        try (CloseableHttpResponse httpResponse = httpClient.execute(new HttpGet(svgFile))) {
            String response = EntityUtils.toString(httpResponse.getEntity());
            Document document = Jsoup.parse(response);
            document.select("image").forEach(element -> {
                String href = element.attr("xlink:href");
                String[] split = href.split("\\/");
                String file = split[split.length - 1];
                href = "img/" + id + "/" + file;
                downloadImage(svg, href);
                element.attr("xlink:href", href);
            });
            Path output = OUTPUT_PATH.resolve(id + ".svg");
            Files.createDirectories(output.getParent());
            Files.write(output, document.toString().getBytes());
            System.out.println("Downloaded " + svgFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static void downloadImage(String svgUrl, String href) {
        String url = svgUrl + href;
        try (CloseableHttpResponse httpResponse = httpClient.execute(new HttpGet(url))) {
            byte[] data = EntityUtils.toByteArray(httpResponse.getEntity());
            Path output = OUTPUT_PATH.resolve(href);
            Files.createDirectories(output.getParent());
            Files.write(output, data);
            System.out.println("Downloaded image " + url);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private static class EasyCookieSpec extends DefaultCookieSpec {
        
        @Override
        public void validate(Cookie arg0, CookieOrigin arg1) throws MalformedCookieException {
            //allow all cookies
        }
    }
}
