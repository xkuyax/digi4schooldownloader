Usage: run the program with the following arguments java -jar digi4schooldownloader.jar user password bookId pages

The program downloads all svg and img files to the data directory. Before the progam starts, the data folder gets cleared. 

After the program has finished, cd into the data folder and run the ./convert.sh in the windows 10 ubuntu bash. You might need to install rsvg-convert which can be installed by executing sudo apt-get update and sudo apt-get install librsvg2-bin